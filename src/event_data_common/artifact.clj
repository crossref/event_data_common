(ns event-data-common.artifact
  "Retrieve things from the Artifact Repository."
  (:require [robert.bruce :refer [try-try-again]]
            [clojure.tools.logging :as log]
            [clj-http.client :as client]
            [clojure.data.json :as json]
            [config.core :refer [env]]
            [clojure.string]
            [clojure.java.io :as io]))

(defn fetch-local-index
  "Retrieve the Artifact index from the resources."
  []
  (json/read-str (slurp (io/resource "artifacts/a/artifacts.json"))))

(defn fetch-index
  "Retrieve the Artifact index."
  []
  (let [the-path (str (:global-artifact-url-base env) "/a/artifacts.json")]
    (try 
      (try-try-again {:sleep 10000 :tries 10}
         #(let [result (client/get the-path)
                parsed (when-let [body (:body result)] (json/read-str body))]
           (when-not (= (:status result) 200)
             (log/error "Can't get Artifact list from" the-path "response status:" (:status result))
             ; Exception caught by try-try-again n times. 
             (throw (new Exception "Couldn't get Artifact list.")))
           parsed))
     (catch Exception e
      (do 
        (log/error "Gave up getting Artifact List" the-path "response" e)
        (log/info "Will revert to using local copy of Artifact list")
        (fetch-local-index))))))


(defn fetch-artifact-names
  []
  (keys (get-in (fetch-index) ["artifacts"])))

(defn fetch-latest-version-link
  "Get the URL of the latest version of the Artifact, or nil."
  [artifact-name]
  (get-in (fetch-index) ["artifacts" artifact-name "current-version-link"]))

(defn fetch-local-latest-artifact
  "Get the content of the latest version of the artifact from the resources."
  [the-path typ]
  (let [local-path (clojure.string/replace the-path (:global-artifact-url-base env) "artifacts")]
    (case typ 
      :text (slurp (io/resource local-path))
      :stream (io/input-stream (io/resource local-path)))))

(defn fetch-latest-artifact
  "Get the content of the latest version of the artifact.
   typ should be one of [:stream or :text]
   Return tuple of [version-url, content]"
   [artifact-name typ]
   (when-let [the-path (fetch-latest-version-link artifact-name)]
    (try 
      (try-try-again {:sleep 10000 :tries 10}
         #(let [result (client/get the-path {:as typ})]
           (when-not (= (:status result) 200)
            
             (log/error "Can't get Artifact list from" the-path "response status:" (:status result))
             ; Exception caught by try-try-again n times. 
             (throw (new Exception "Couldn't get Artifact list.")))

           (:body result)))
     (catch Exception e
      (do
        (log/error "Gave up getting Artifact List" the-path "response" e)
        (log/info "Will revert to using local copy of artifact")
        (fetch-local-latest-artifact the-path typ))))))

(defn fetch-latest-artifact-string
  "Get the content of the latest version of the artifact as a string. Only for small artifacts!"
  [artifact-name] 
  (fetch-latest-artifact artifact-name :text))


(defn fetch-latest-artifact-stream
  "Get the content of the latest version of the artifact as a stream."
  [artifact-name] 
  (fetch-latest-artifact artifact-name :stream))



